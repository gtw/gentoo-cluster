"""A group of x86 experimental nodes running Gentoo.

Instructions:
You get linux 5.15, gcc 11.2, perl 5.34, python 2.7 and 3.9.
"""

import geni.portal as portal

pc = portal.Context()
request = pc.makeRequestRSpec()

pc.defineParameter( "numnodes", "Number of nodes",
                    portal.ParameterType.INTEGER, 1 )
pc.defineParameter( "nodetype",  "Node type (optional)",
                    portal.ParameterType.STRING, "" )
pc.defineParameter( "vm", "Use VMs",
                    portal.ParameterType.BOOLEAN, False )

params = pc.bindParameters()

if params.numnodes < 1:
    pc.reportError( portal.ParameterError( "Need at least 1 node.",
                                           [ "numnodes" ] ) )
pc.verifyParameters()

if params.numnodes > 1:
    if params.numnodes == 2:
        lan = request.Link()
    else:
        lan = request.LAN()

for i in range( params.numnodes ):
    if params.vm:
        name = "vm" + str( i )
        node = request.XenVM( name )
    else:
        name = "node" + str( i )
        node = request.RawPC( name )

    node.disk_image = "urn:publicid:IDN+emulab.net+image+tbres//gentoo-base"
    
    if params.numnodes > 1:
        iface = node.addInterface( "eth1" )
        lan.addInterface( iface )

    if params.nodetype != "":
        node.hardware_type = params.nodetype

pc.printRequestRSpec(request)
